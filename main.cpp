#include "optionparser.h"
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include "server.h"
#include "parser.h"


int main(int argc, char* argv[]) {
	int rounds;
	string first, second;
	parserBattleOfShips::parse(argc, argv, &rounds, &first, &second);

	Game game{};
	game.run(rounds, first, second);
	return 0;
}
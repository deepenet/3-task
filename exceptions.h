#pragma once
#include <exception>

enum shipType : int{ BOAT = 0, DESTROYER, CRUISER, BATTLESHIP };

enum playerTurn : int{ PLAYER_1 = 0, PLAYER_2 = 1 };

enum cellStatus : int{ EMPTY = 0, SHOOTED, SHIP, SHIP_DAMAGED };

enum puttingShip : int{ CELL_IS_FULL = 0, PLACED, OUT_OF_FIELD, SHIPS_OVER, WRONG_DIRECTION, ALL_SHIPS_OVER };

enum fieldType : int{ MY = 0, ENEMY = 1 };

enum shootStatus : int{ MISS = 0, GOT, CANT_SHOOT, KILLED };

class cantShootThisCell : std::exception {
	const char* what() const noexcept {
		return "Can't shoot to this cell.\n";
	}
};

class outOfField : std::exception {
	const char* what() const noexcept {
		return "Out of field.\n";
	}
};

class shipsOver : std::exception {
	const char* what() const noexcept {
		return "Ships this size over.\n";
	}
};

class playerIsNotExist : std::exception {
	const char* what() const noexcept {
		return "Player is not exist.\n";
	}
};
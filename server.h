#pragma once
namespace {
	const int sizeField = 10;
}
#include "exceptions.h"
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <map>
#include "gamer.h"
#include "view.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::make_pair;

struct Ship {
	int type;
	int health;
};

class Game {
	Gamer *first, *second;
	struct Cell {
		Ship *shipPtr = nullptr;
		cellStatus status = EMPTY;
	};
	// field[player][type field][x][y]
	Cell field[2][2][sizeField][sizeField];
	// numbShips[player][ship type]
	int numbShips[2][4] = {};
	bool betweenBorders(int coord, int left = 0, int right = 9);
	Ship* allShips[(4+3*2+2*3+4)*2] = {nullptr};
	int counterShip = 0;
	bool isAllShipsPlaced(Gamer *gamer);
	string whosWin();

public:
	~Game();
	shootStatus doShoot(Gamer *gamer, std::pair<int, int> coord);
	puttingShip putShip(Gamer *gamer, std::pair<int, int> coord, int direction, int type);
	// TODO: Gamer *gamer -> int hash
	void getField(int player, char fieldOut[sizeField][sizeField], fieldType type);
	void getField(Gamer *gamer, char fieldOut[sizeField][sizeField], fieldType type);
	//
	void refresh();
	void run(int count, const string &first, const string &second);
};
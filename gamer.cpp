#include "gamer.h"

void ConsoleGamer::fillField(Game &game) {
	int y, direction, type;
	char x;
	string inputData;
	puttingShip placeStatus = CELL_IS_FULL;
	while(placeStatus == CELL_IS_FULL) {
		cout << "Enter coord (begining of ship) - pair of letter and number <x, y>, direction (0 - from right to left; 1 - from up to down) and size of ship:\n";
		while(true) {
			std::getline(cin, inputData);
			if(inputData.size() != 4) {
				cout << "Wrong number of paremeters (enter without spaces)\n";
			} else {
				x = inputData[0];
				y = inputData[1] - '0';
				direction = inputData[2] - '0';
				type = inputData[3] - '0';
				if(x < 'a' || x > 'j' || y < 0 || y > 9)
					cout << "Entered place of ship is out of field, try again\n";
				else 
					break;
			}
		}
		placeStatus = game.putShip(this, std::make_pair(x - 'a', y), direction, type - 1);
		switch(placeStatus) {
			case OUT_OF_FIELD:
				cout << "Entered place of ship is out of field, try again\n";
				break;
			case CELL_IS_FULL:
				cout << "Ship can not to be placed around/in another ship(-s)\n";
				break;
			case SHIPS_OVER:
				cout << "All ships this type have already placed\n";
				break;
		}
	}
}

int ConsoleGamer::shoot(Game &game) {
	shootStatus status = CANT_SHOOT;
	string inputData;
	char x;
	int y;
	while(status == CANT_SHOOT) {
		cout << "Enter coord - pair of numbers <x, y>:\n";
		std::getline(cin, inputData);
		if(inputData.size() != 2) {
			cout << "Wrong number of paremeters (enter without spaces)\n";
		} else {
			x = inputData[0];
			y = inputData[1];
			status = game.doShoot(this, make_pair(x - 'a', y - '0'));
			if(status == CANT_SHOOT) {
				cout << "You can not shoot " << static_cast<int>(x - 'a') << ' ' << static_cast<int>(y - '0') << " cell\n";
			}
		}
	}
	return status;
}

std::pair<int, int> RandomGamer::randomChooseCoord(int borderX , int borderY) {
	std::srand(clock() + std::time(0));
	int x = std::rand()%borderX;
	int y = std::rand()%borderY;
	return std::make_pair(x, y);
}

void RandomGamer::fillField(Game &game) {
	std::srand(clock() + std::time(0));
	int counter = 1;
	// 4 cells
	while(counter) {
		int direction = std::rand()%2;
		std::pair<int, int> firstCell = randomChooseCoord(direction == 0 ? 7 : 10, direction == 0? 10 : 7);
		if(game.putShip(this, firstCell, direction, BATTLESHIP) == PLACED) {
			counter--;
		} else {
			//TODO: print error
		}
	}
	// 3 cells
	counter = 2;
	while(counter) {
		int direction = std::rand()%2;
		std::pair<int, int> firstCell = randomChooseCoord(direction == 0 ? 8 : 10, direction == 0 ? 10 : 8);
		if(game.putShip(this, firstCell, direction, CRUISER))
			counter--;
	}
	// 2 cells
	counter = 3;
	while(counter) {
		int direction = std::rand()%2;
		std::pair<int, int> firstCell = randomChooseCoord(direction == 0 ? 9 : 10, direction == 0 ? 10 : 9);
		if(game.putShip(this, firstCell, direction, DESTROYER)) {
			counter--;
		}
	}
	// 1 cells
	counter = 4;
	while(counter) {
		int direction = std::rand()%2;
		std::pair<int, int> firstCell = randomChooseCoord();
			if(game.putShip(this, firstCell, direction, BOAT))
				counter--;
	}
}

int RandomGamer::shoot(Game &game) {
	shootStatus got = CANT_SHOOT;
	std::pair<int, int> coord;
	while(got == CANT_SHOOT) {
		coord = randomChooseCoord();
		got = game.doShoot(this, coord);
	}
	return got;
}

std::pair<int, int> OptimalGamer::randomChooseCoord(int borderX, int borderY) {
	std::srand(clock() + std::time(0));
	int x = std::rand()%borderX;
	int y = std::rand()%borderY;
	return std::make_pair(x, y);
}

std::pair<int, int> OptimalGamer::randomChooseBorderCoord(int &direction) {
	int x, y;
	std::srand(clock() + std::time(0));
	direction = std::rand()%2;
	if(!direction) {
		x = std::rand()%10;
		y = (std::rand()%2)*9;
	} else {
		x = (std::rand()%2)*9;
		y = std::rand()%10;
	}
	return std::make_pair(x, y);
}

void OptimalGamer::fillField(Game &game) {
	std::srand(clock() + std::time(0));
	int counter = 1;
	// 4 cells
	while(counter) {
		int direction;
		std::pair<int, int> firstCell = randomChooseBorderCoord(direction);
		if(game.putShip(this, firstCell, direction, BATTLESHIP) == PLACED)
			counter--;
	}
	// 3 cells
	counter = 2;
	while(counter) {
		int direction;
		std::pair<int, int> firstCell = randomChooseBorderCoord(direction);
		if(game.putShip(this, firstCell, direction, CRUISER) == PLACED)
			counter--;
	}
	// 2 cells
	counter = 3;
	while(counter) {
		int direction;
		std::pair<int, int> firstCell = randomChooseBorderCoord(direction);
		if(game.putShip(this, firstCell, direction, DESTROYER) == PLACED) {
			counter--;
		}
	}
	// 1 cells
	counter = 4;
	while(counter) {
		int direction = 0;
		std::pair<int, int> firstCell = randomChooseCoord();
		int status = game.putShip(this, firstCell, direction, BOAT);
		if(status == PLACED || status == ALL_SHIPS_OVER)
			counter--;
	}
}

int OptimalGamer::shoot(Game &game) {
	shootStatus got = CANT_SHOOT;
	while(got == CANT_SHOOT) {
		if(existDamagedShip) {
			if(directionShooting.first || directionShooting.second) {
				got = game.doShoot(this, make_pair(coordLastDamagedShip.first + directionShooting.first, coordLastDamagedShip.second + directionShooting.second));
				if(got == GOT || got == KILLED) {
					coordLastDamagedShip.first += directionShooting.first;
					coordLastDamagedShip.second += directionShooting.second;
					sizeLastKilled++;
				}
				if(got != GOT && got != KILLED) {
					directionShooting.first *= -1;
					directionShooting.second *= -1;
					coordLastDamagedShip = coordBeginShipDamagind;
				} else if(got == KILLED) {
					killedBigShips++;
					directionShooting.first = 0;
					directionShooting.second = 0;
					if(sizeLastKilled == 4) {
						typeAim = DESTROYER;
					}
					existDamagedShip = false;
					sizeLastKilled = 0;
				}
			} else {
				int x, y;
				while(got == CANT_SHOOT) {
					std::srand(clock() + std::time(0));
					// choose coord shift: up, left, down or right
					do {
						x = rand()%3 - 1;
						y = rand()%3 - 1;
					// x*y != 0 means <x, y> shift is not parallel ox and oy
					} while(x*y);
					got = game.doShoot(this, make_pair(coordLastDamagedShip.first + x, coordLastDamagedShip.second + y));
				}
				if(got == GOT) {
					sizeLastKilled++;
					coordLastDamagedShip.first += x;
					coordLastDamagedShip.second += y;
					directionShooting.first = x;
					directionShooting.second = y;
				} else if(got == KILLED) {
					sizeLastKilled = 0;
					killedBigShips++;
					existDamagedShip = false;
				}
			}
			if(killedBigShips == 6) {
				typeAim = BOAT;
			}
		} else {
			switch(typeAim) {
				case BATTLESHIP:
					while(got == CANT_SHOOT) {
						do {
							coord = randomChooseCoord();
						} while((coord.first + coord.second + 1)%4 != 0);
						got = game.doShoot(this, coord);
					}
					if(got == GOT) {
						coordLastDamagedShip = coordBeginShipDamagind = coord;
						existDamagedShip = true;
						sizeLastKilled++;
					}
					if(GOT == KILLED) {
						existDamagedShip = false;
					}
					break;
				case DESTROYER:
					while(got == CANT_SHOOT) {
						do {
							coord = randomChooseCoord();
						} while((coord.first + coord.second + 1)%2 != 0);
						got = game.doShoot(this, coord);
					}
					if(got == GOT) {
						coordLastDamagedShip = coordBeginShipDamagind = coord;
						existDamagedShip = true;
						sizeLastKilled++;
					}
					if(GOT == KILLED) {
						existDamagedShip = false;
					}
					break;
				case BOAT:
					coord = randomChooseCoord();
					got = game.doShoot(this, coord);
					break;
			}
		}
	}
	return got;
}
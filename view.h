#pragma once
#include "server.h"
#include "gamer.h"

class Gamer;
class Game;

class GameView {
protected:
	Gamer *player;
	char myField[sizeField][sizeField];
	char enemyField[sizeField][sizeField];
public:
	GameView(Gamer *gamer) {
		player = gamer;
	}
	virtual void showMyField(Game &game) = 0;
	virtual void showEnemyField(Game &game) = 0;
	virtual void showBothEnemyField(Game &game) = 0;
};

class ConsoleView : public GameView {
public:
	ConsoleView(Gamer *gamer) : GameView(gamer) {}
	void showMyField(Game &game);
	void showEnemyField(Game &game);
	void showBothEnemyField(Game &game);
};
#include "parser.h"

bool parserBattleOfShips::parse(int argc, char** argv, int *rounds, std::string *first, std::string *second) {
	argc -= (argc > 0); argv += (argc > 0); // skip program name argv[0] if present
	option::Stats stats(usage, argc, argv);
	option::Option options[stats.options_max], buffer[stats.buffer_max];
	option::Parser parse(usage, argc, argv, options, buffer);
	if (parse.error())
		return false;
	optionParameters optionParam{};
	if (options[HELP]) {
		int columns = getenv("COLUMNS")? atoi(getenv("COLUMNS")) : 80;
		option::printUsage(fwrite, stdout, usage, columns);
		*rounds = 0;
		*first = "";
		*second = "";
		return true;
	}
	bool countTaken = false;
	bool firstTaken = false;
	bool secondTaken = false;
	string mod;
	for (int i = 0; i < parse.optionsCount(); ++i) {
		option::Option& opt = buffer[i];
		switch (opt.index()) {
			case HELP:
				// not possible, because handled further above and exits the program
			case COUNT:
				countTaken = true;
				optionParam.setRounds(atoi(opt.arg) + optionParam.getRounds());
				if(atoi(opt.arg) < 1) {
					cout << "Wrong number of rounds!" << endl;
					return false;
				}
				break;
			case FIRST:
				mod = opt.arg;
				if(mod != "random" && mod != "optimal" && mod != "console") {
					cout << "Wrong mod of first player!" << endl;
					return false;
				}
				firstTaken = true;
				optionParam.setFirst(opt.arg);
				break;
			case SECOND:
				mod = opt.arg;
				if(mod != "random" && mod != "optimal" && mod != "console") {
					cout << "Wrong mod of second player!" << endl;
					return false;
				}
				secondTaken = true;
				optionParam.setSecond(opt.arg);
				break;
			case UNKNOWN:
				// not possible because Arg::Unknown returns ARG_ILLEGAL
				// which aborts the parse with an error
			break;
		}
	}
	if(!countTaken)
		optionParam.setRounds(1);
	if(!firstTaken)
		optionParam.setFirst("random");
	if(!secondTaken)
		optionParam.setSecond("random");
	cout << "Number of rounds: " << optionParam.getRounds() << endl << "First player: " << optionParam.getFirst() << endl << "Second player: " << optionParam.getSecond() << endl;
	*rounds = optionParam.getRounds();
	*first = optionParam.getFirst();
	*second = optionParam.getSecond();
	return true;
}
#include "view.h"

string colorSymb(char symb) {
	string out;
	out[0] = symb;
	switch(symb)  {
		case 'X':
			return "\033[01;31m\X\033[00m";
		case 'S':
			return "\033[01;33m\S\033[00m";
		case '*':
			return "\033[01;36m\*\033[00m";
		case ' ':
			return " ";
		default: 
			return out;
	}
}

void ConsoleView::showMyField(Game &game) {
	game.getField(player, myField, MY);
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
	for(int i = 0; i < sizeField; i++) {
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m";
		//print field
		for(int j = 0; j < sizeField; j++) {
			cout << ' ' << colorSymb(myField[j][i]);
		}
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m" << endl;
	}
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
}

void ConsoleView::showEnemyField(Game &game) {
	game.getField(player, enemyField, ENEMY);
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
	for(int i = 0; i < sizeField; i++) {
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m";
		for(int j = 0; j < sizeField; j++) {
			cout << ' ' << colorSymb(enemyField[j][i]);		
		}
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m" << endl;
	}
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
}

void ConsoleView::showBothEnemyField(Game &game) {
	game.getField(PLAYER_1, myField, ENEMY);
	game.getField(PLAYER_2, enemyField, ENEMY);
	cout << "    \033[01;32mFIRST'S PLAYER     \t\t    SECOND'S PLAYER\033[00m\n";
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << "   \t  ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
	for(int i = 0; i < sizeField; i++) {
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m";
		for(int j = 0; j < sizeField; j++) {
			cout << ' ' << colorSymb(myField[j][i]);
		}
		cout << " \033[01;35m" << i << '\t' << i << "\033[00m";
		for(int j = 0; j < sizeField; j++) {
			cout << ' ' << colorSymb(enemyField[j][i]);
		}
		//print y coordinate
		cout << " \033[01;35m" << i << "\033[00m" << endl;
	}
	//print x coordinate
	cout << "   ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << "   \t  ";
	for(int i = 0; i < sizeField; i++) {
		cout << "\033[01;35m" << static_cast<char>('a' + i) << "\033[00m" << ' ';
	}
	cout << endl;
}
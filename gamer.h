#pragma once
#include "server.h"

class Game;

class Gamer {
public:
	virtual int shoot(Game &game) = 0;
	virtual void fillField(Game &game) = 0;
};

class ConsoleGamer : public virtual Gamer {
public:
	void fillField(Game &game);
	int shoot(Game &game);
};

class RandomGamer : public virtual Gamer {
	std::pair<int, int> randomChooseCoord(int borderX = 10, int borderY = 10);
public:
	void fillField(Game &game);
	int shoot(Game &game);
};

class OptimalGamer : public virtual Gamer {
	int sizeLastKilled;
	int killedBigShips;
	int typeAim;
	bool existDamagedShip;
	std::pair<int, int> coordBeginShipDamagind;
	std::pair<int, int> coordLastDamagedShip;
	std::pair<int, int> directionShooting;
	std::pair<int, int> coord;
	std::pair<int, int> randomChooseCoord(int borderX = 10, int borderY = 10);
	std::pair<int, int> randomChooseBorderCoord(int &direction);
public:
	OptimalGamer() {
		sizeLastKilled = 0;
		killedBigShips = 0;
		typeAim = BATTLESHIP;
		existDamagedShip = false;
		coordBeginShipDamagind = std::make_pair(0, 0);
		coordLastDamagedShip = std::make_pair(0, 0);
		directionShooting = std::make_pair(0, 0);
		coord = std::make_pair(0, 0);
	}
	void fillField(Game &game);
	int shoot(Game &game);
};

class gamerFactory {
public:    
	//create object of operation class
	virtual Gamer* createGamer() = 0;
};

class randomGamerFactory: public virtual gamerFactory {
public:    
	Gamer* createGamer() {
		return new RandomGamer{};
	}
};

class consoleGamerFactory: public virtual gamerFactory {
public:    
	Gamer* createGamer() {
		return new ConsoleGamer{};
	}
};

class optimalGamerFactory: public virtual gamerFactory {
public:    
	Gamer* createGamer() {
		return new OptimalGamer{};
	}
};
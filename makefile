CC = g++

CFlags = -O3 -std=c++17

all: build

build: main.o parser.o server.o gamer.o view.o
	$(CC) main.o parser.o server.o gamer.o view.o -o main

main.o: main.cpp
	$(CC) $(CFlags) main.cpp -c
	
parser.o: parser.cpp
	$(CC) $(CFlags) parser.cpp -c
	
server.o: server.cpp
	$(CC) $(CFlags) server.cpp -c
	
gamer.o: gamer.cpp
	$(CC) $(CFlags) gamer.cpp -c

view.o: view.cpp
	$(CC) $(CFlags) view.cpp -c

clear:
	rm *.o
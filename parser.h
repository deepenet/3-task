#pragma once
#include "optionparser.h"
#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

	class optionParameters {
		int rounds;
		string first;
		string second;
	public:
		void setRounds(int value) {
			rounds = value;
		}
		int getRounds() {
			return rounds;
		}
		int decrement() {
			return --rounds;
		}
		void setFirst(string value) {
			first = value;
		}
		void setSecond(string value) {
			second = value;
		}
		string getFirst() {
			return first;
		}
		string getSecond() {
			return second;
		}
	};

	struct Arg: public option::Arg {
		static void printError(const char* msg1, const option::Option& opt, const char* msg2) {
			fprintf(stderr, "%s", msg1);
			fwrite(opt.name, opt.namelen, 1, stderr);
			fprintf(stderr, "%s", msg2);
		}
		static option::ArgStatus Unknown(const option::Option& option, bool msg) {
			if (msg) printError("Unknown option '", option, "'\n");
			return option::ARG_ILLEGAL;
		}
		//Must have an argument
		static option::ArgStatus Required(const option::Option& option, bool msg) {
			if (option.arg != 0)
				return option::ARG_OK;
			return option::ARG_NONE;
		}
		//Can NOT take the empty string as argument
		static option::ArgStatus NonEmpty(const option::Option& option, bool msg) {
			if (option.arg != 0 && option.arg[0] != 0)
				return option::ARG_OK;
			if (msg) printError("Option '", option, "' requires a non-empty argument\n");
				return option::ARG_ILLEGAL;
		}
		//Requires a number as argument
		static option::ArgStatus Numeric(const option::Option& option, bool msg) {
			char* endptr = 0;
			if (option.arg != 0 && strtol(option.arg, &endptr, 10));
			if (endptr != option.arg && *endptr == 0)
				return option::ARG_OK;
			if (msg) printError("Option '", option, "' requires a numeric argument\n");
				return option::ARG_ILLEGAL;
		}
	};

	enum  optionIndex { UNKNOWN, HELP, COUNT, FIRST, SECOND };
	const option::Descriptor usage[] =
	{
		{UNKNOWN, 0, "" , ""    ,	Arg::None																		},
		{HELP,    0, "h", "help",	Arg::None, 		"  --help,         -h \tPrint usage and exit." 					},
		{COUNT,   0, "c", "count",	Arg::Numeric, 	"  --count <arg>,  -c \tNumber rounds in series. Default: 1." 	},
		{FIRST,   0, "f", "first",	Arg::Required, 	"  --first <arg>,  -f \tType of first player. Default: random. "
													"Console session: console. Optimal strategy: optimal."			},
		{SECOND,  0, "s", "second",	Arg::Required, 	"  --second <arg>, -s \tType of second player. Default: random."	
													"Console session: console. Optimal strategy: optimal."			},
		{0,0,0,0,0,0}
	};

namespace parserBattleOfShips {
	bool parse(int argc, char** argv, int *rounds, std::string *first, std::string *second);
}
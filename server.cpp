#include "server.h"

void wait(int millisec) {
	int counter;
	clock_t start = clock();
	while ((clock() - start)/(CLOCKS_PER_SEC/1000) < millisec) {
		counter++;
	}
}

bool Game::betweenBorders(int coord, int left, int right) {
	if(coord < left || coord > right)
		return false;
	return true;
}

Game::~Game() {
	for(int i = 0; i < (4+3*2+2*3+4)*2; i++) {
		delete allShips[i];
	}
}
	
bool Game::isAllShipsPlaced(Gamer *gamer) {
	int player;
	if(gamer == first) {
		player = PLAYER_1;
	} else if(gamer == second) {
		player = PLAYER_2;
	} else {
		throw playerIsNotExist{};
	}
	if(numbShips[player][BOAT] == 4 && numbShips[player][DESTROYER] == 3 && numbShips[player][CRUISER] == 2 && numbShips[player][BATTLESHIP] == 1)
		return true;
	return false;
}
	
string Game::whosWin() {
	if(!numbShips[PLAYER_1][BOAT] && !numbShips[PLAYER_1][DESTROYER] && !numbShips[PLAYER_1][CRUISER] && !numbShips[PLAYER_1][BATTLESHIP])
		return "second";
	if(!numbShips[PLAYER_2][BOAT] && !numbShips[PLAYER_2][DESTROYER] && !numbShips[PLAYER_2][CRUISER] && !numbShips[PLAYER_2][BATTLESHIP])
		return "first";
	return "";
}

shootStatus Game::doShoot(Gamer *gamer, std::pair<int, int> coord) {
	int player;
	if(gamer == first) {
		player = PLAYER_1;
	} else if(gamer == second) {
		player = PLAYER_2;
	} else {
		throw playerIsNotExist{};
	}
	if(coord.first > 9 || coord.second > 9 || coord.first < 0 || coord.second < 0)
		return CANT_SHOOT;
	// if got it
	if(field[player xor 1][MY][coord.first][coord.second].status == SHIP) {
		field[player xor 1][MY][coord.first][coord.second].status = SHIP_DAMAGED;
		(field[player xor 1][MY][coord.first][coord.second].shipPtr->health)--;
		field[player][ENEMY][coord.first][coord.second].status = SHIP_DAMAGED;
		// if ship is dead
		if(field[player xor 1][MY][coord.first][coord.second].shipPtr->health == 0) {
			numbShips[player xor 1][field[player xor 1][MY][coord.first][coord.second].shipPtr->type]--;
			std::pair<int, int> leftUp = field[player xor 1][MY][coord.first][coord.second].shipPtr->coord.first;
			std::pair<int, int> rightDown = field[player xor 1][MY][coord.first][coord.second].shipPtr->coord.second;
			leftUp.first--;
			leftUp.second--;
			rightDown.first++;
			rightDown.second++;
			for(int i = leftUp.first; i <= rightDown.first; i++) {
				for(int j = leftUp.second; j <= rightDown.second; j++) {
					//if inside the field
					if(!(i < 0) && !(i > 9) && !(j < 0) && !(j > 9)) {
						// if it was not a ship
						if(field[player xor 1][MY][i][j].status != SHIP_DAMAGED) {
							field[player xor 1][MY][i][j].status = SHOOTED;
							field[player][ENEMY][i][j].status = SHOOTED;
						}
					}
				}
			}
			return KILLED;
		}
		return GOT;
	}
	// if miss
	if(field[player xor 1][MY][coord.first][coord.second].status == EMPTY) {
		field[player xor 1][MY][coord.first][coord.second].status = SHOOTED;
		field[player][ENEMY][coord.first][coord.second].status = SHOOTED;
		return MISS;
	}
	return CANT_SHOOT;
}


puttingShip Game::putShip(Gamer *gamer, std::pair<int, int> coord, int direction, int type) {
	int player;
	if(gamer == first) {
		player = PLAYER_1;
	} else if(gamer == second) {
		player = PLAYER_2;
	} else {
		throw playerIsNotExist{};
	}
	Ship *tmp;
	// if ships this size over
	if(numbShips[player][type] == (4 - type))
		return SHIPS_OVER;
	if(coord.first < 0 || coord.second < 0) {
		return OUT_OF_FIELD;
	}
	switch(direction) {
		case 0: 
			if(coord.first + type > 9)
				return OUT_OF_FIELD;
			for(int i = -1; i <= type + 1; i++) {
				for(int j = -1; j < 2; j++) {
					if(betweenBorders(coord.first + i) && betweenBorders(coord.second + j) && field[player][MY][coord.first + i][coord.second + j].status != EMPTY) {
						return CELL_IS_FULL;
					}
				}
			}
			tmp = new Ship;
			tmp->health = type + 1;
			tmp->type = type;
			tmp->coord = make_pair(make_pair(coord.first, coord.second), make_pair(coord.first + type, coord.second));
			for(int i = 0; i <= type; i++) {
				field[player][MY][coord.first + i][coord.second].status = SHIP;
				field[player][MY][coord.first + i][coord.second].shipPtr = tmp;
			}
			allShips[counterShip] = tmp;
			counterShip++;
			numbShips[player][type]++;
			if(numbShips[player][BOAT] == 4 && numbShips[player][DESTROYER] == 3 && numbShips[player][CRUISER] == 2 && numbShips[player][BATTLESHIP] == 1) {
				return ALL_SHIPS_OVER;
			} else {
				return PLACED;
			}
		case 1:
			if(coord.second + type > 9)
				return OUT_OF_FIELD;
			for(int j = -1; j <= type + 1; j++) {
				for(int i = -1; i < 2; i++) {
					if(betweenBorders(coord.first + i) && betweenBorders(coord.second + j) && field[player][MY][coord.first + i][coord.second + j].status != EMPTY) {
						return CELL_IS_FULL;
					}
				}
			}
			tmp = new Ship;
			tmp->health = type + 1;
			tmp->type = type;
			tmp->coord = make_pair(make_pair(coord.first, coord.second), make_pair(coord.first, coord.second + type));
			for(int i = 0; i <= type; i++) {
				field[player][MY][coord.first][coord.second + i].status = SHIP;
				field[player][MY][coord.first][coord.second + i].shipPtr = tmp;
			}
			allShips[counterShip] = tmp;
			counterShip++;
			numbShips[player][type]++;
			if(numbShips[player][BOAT] == 4 && numbShips[player][DESTROYER] == 3 && numbShips[player][CRUISER] == 2 && numbShips[player][BATTLESHIP] == 1) {
				return ALL_SHIPS_OVER;
			} else {
				return PLACED;
			}
		default: return WRONG_DIRECTION;
	}
}

void Game::getField(Gamer *gamer, char fieldOut[sizeField][sizeField], fieldType type) {
	int player;
	if(gamer == first) {
		player = PLAYER_1;
	} else if(gamer == second) {
		player = PLAYER_2;
	} else {
		throw playerIsNotExist{};
	}
	for(int i = 0; i < sizeField; i++) {
		for(int j = 0; j < sizeField; j++) {
			switch(field[player][type][i][j].status) {
				case SHIP:
					fieldOut[i][j] = 'S';
					break;
				case SHIP_DAMAGED:
					fieldOut[i][j] = 'X';
					break;
				case SHOOTED:
					fieldOut[i][j] = '*';
					break;
				case EMPTY:
					fieldOut[i][j] = ' ';
					break;
				default:
					fieldOut[i][j] = '/';
			}
		}
	}
}

void Game::getField(int player, char fieldOut[sizeField][sizeField], fieldType type) {
	if(type != ENEMY)
		throw playerIsNotExist{};
	if(player != PLAYER_1 && player != PLAYER_2)
		throw playerIsNotExist{};
	for(int i = 0; i < sizeField; i++) {
		for(int j = 0; j < sizeField; j++) {
			switch(field[player][type][i][j].status) {
				case SHIP:
					fieldOut[i][j] = 'S';
					break;
				case SHIP_DAMAGED:
					fieldOut[i][j] = 'X';
					break;
				case SHOOTED:
					fieldOut[i][j] = '*';
					break;
				case EMPTY:
					fieldOut[i][j] = ' ';
					break;
				default:
					fieldOut[i][j] = '/';
			}
		}
	}
}

void Game::refresh() {
	for(int i = 0; i < sizeField; i++) {
		for(int j = 0; j < sizeField; j++) {
			for(int k = 0; k < 2; k++) {
				field[k][k][i][j].shipPtr = nullptr;
				field[k][k][i][j].status = EMPTY;
				field[k][k xor 1][i][j].shipPtr = nullptr;
				field[k][k xor 1][i][j].status = EMPTY;
			}
		}
	}
	for(int i = 0; i < (4+3*2+2*3+4)*2; i++) {
		delete allShips[i];
		allShips[i] = nullptr;
	}
	for(int i = 0; i < 4; i++) {
		numbShips[0][i] = numbShips[1][i] = 0;
	}
	counterShip = 0;
}

void Game::run(int count, const string &typeFirst, const string &typeSecond) {
	int winsFirst = 0, winsSecond = 0;
	cout << typeFirst << ' ' << typeSecond << endl;
	map<string, gamerFactory*> gamers;
	gamers["random"] = new randomGamerFactory{};
	gamers["console"] = new consoleGamerFactory{};
	gamers["optimal"] = new optimalGamerFactory{};
	for(int i = 0; i < count; i++) {
		Gamer *firstPlayer = (gamers.at(typeFirst))->createGamer();
		Gamer *secondPlayer = (gamers.at(typeSecond))->createGamer();
		first = firstPlayer;
		second = secondPlayer;
		ConsoleView consoleFirst(first);
		ConsoleView consoleSecond(second);
		int queue = 0;
		while(!this->isAllShipsPlaced(first)) {
			system("clear");
			cout << "First is filling the field:\n";
			consoleFirst.showMyField(*this);
			first->fillField(*this);
		}
		while(!(this->isAllShipsPlaced(second))) {
			system("clear");
			cout << "Second is filling the field:\n";
			consoleSecond.showMyField(*this);
			second->fillField(*this);
		}
		bool firstWin = false, secondWin = false;
		wait(10);
		while(true) {
			//check win
			string win = this->whosWin();
			if(win != "") {
				if(win == "first")
					winsFirst++;
				else
					winsSecond++;
				system("clear");
				cout << "First:\n";
				consoleFirst.showMyField(*this);
				cout << "\nSecond:\n";
				consoleSecond.showMyField(*this);
				cout << win;
				cout << "\n\n\nPress any key to continue.\n";
				getc(stdin);
				delete firstPlayer;
				delete secondPlayer;
				refresh();
				break;
			} else {
				int result;
				system("clear");
				consoleFirst.showBothEnemyField(*this);
				wait(5);
				switch(queue) {
					case 0:
						cout << "First player's turn:\n";
						result = first->shoot(*this);
						(result == GOT || result == KILLED) ? queue = 0 : queue = 1;
						break;
					case 1:
						cout << "Second player's turn:\n";
						result = second->shoot(*this);
						(result == GOT || result == KILLED) ? queue = 1 : queue = 0;
						break;
				}
			}
		}
	}
	cout << "\n\n\t\tFirst won " << winsFirst << " times.\n\t\tSecond won " << winsSecond << " times.\n";
	delete gamers["random"];
	delete gamers["optimal"];
	delete gamers["console"];
}